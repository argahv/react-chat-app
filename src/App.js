import React from 'react'
import Chatkit from '@pusher/chatkit-client'
import MessageList from './Components/MessageList'
import SendMessageForm from './Components/SendMessageForm'
import RoomList from './Components/RoomList'
import NewRoomForm from './Components/NewRoomForm'

import { tokenUrl, instanceLocator } from './config'

class App extends React.Component {

    constructor() {
        super()
        this.state = {
            messages: [],
            roomId: [],
            joinableRooms: [],
            joinedRooms: []
        }

    }

    componentDidMount() {
        const chatManager = new Chatkit.ChatManager({
            instanceLocator,
            userId: 'raghav',
            tokenProvider: new Chatkit.TokenProvider({
                url: tokenUrl
            })
        })

        chatManager.connect()
            .then(currentUser => {
                this.currentUser = currentUser
                this.getRooms()
            })
            .catch(err => console.log('error connecting', err))

    }

    sendMessage = (text) =>
        this.currentUser.sendSimpleMessage({
            text,
            roomId: this.state.roomId
        }).then(messageId => {
            console.log('Added ', messageId)
        })

    getRooms = () => {
        this.currentUser.getJoinableRooms()
            .then(joinableRooms => {
                this.setState({
                    joinableRooms,
                    joinedRooms: this.currentUser.rooms
                })
            })
            .catch(err => console.log('error on joining rooms ', err))
    }

    subscribeToRoom = (roomId) => {
        this.setState({ messages: [] })
        this.currentUser.subscribeToRoom({
            roomId,
            hooks: {
                onMessage: message => {
                    this.setState({
                        messages: [...this.state.messages, message]
                    })
                    console.log('message.text: ', message.text);
                }
            }

        })
            .then(room => {
                this.setState({
                    roomId: room.id
                })
                this.getRooms()
            })
            .catch(err => console.log('error on subscribing to the room ', err))
    }




    createRoom = (name) => {
        this.currentUser.createRoom({
            name
        })
            .then(room => this.subscribeToRoom(room.id))
            .catch(err => console.log('error creating group', err))
    }


    render() {
        return (
            <div className="app" >
                <RoomList
                    roomId={this.state.roomId}
                    subscribeToRoom={this.subscribeToRoom}
                    rooms={[
                        ...this.state.joinableRooms
                        , ...this.state.joinedRooms
                    ]}
                />
                <MessageList
                    roomId={this.state.roomId}
                    joinedRooms={this.state.joinedRooms}
                    rooms={[...this.state.joinedRooms
                    ]}
                    messages={this.state.messages} />
                <SendMessageForm sendMessage={this.sendMessage} />
                <NewRoomForm createRoom={this.createRoom} />
            </div>
        );
    }
}

export default App