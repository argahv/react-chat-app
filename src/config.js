const tokenUrl = "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/ea67ff59-d1f5-4b27-afdb-95f196e792a2/token";
const instanceLocator = "v1:us1:ea67ff59-d1f5-4b27-afdb-95f196e792a2";

export { tokenUrl, instanceLocator }