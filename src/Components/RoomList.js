import React from 'react'

class RoomList extends React.Component {
    render() {
        const orderedRooms = [...this.props.rooms].sort((a, b) => a.id - b.id)
        return (
            <div className="rooms-list">
                <h3>Your Groups:</h3>
                <ul>
                    {orderedRooms.map(room => {
                        const active = this.props.roomId === room.id ? "active" : "";
                        return (
                            <li key={room.id} className={`room ${active}`}>
                                <button
                                    className='groups-button'
                                    activeclassname='isActive'
                                    onClick={() => this.props.subscribeToRoom(room.id)}
                                >
                                    {room.name}
                                </button>
                            </li>
                        )
                    })}
                </ul>

            </div>
        )
    }
}

export default RoomList