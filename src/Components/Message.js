import React from 'react'

const Message = (props) => (
    <div className="message">
        <div className="message-username">
            <h1>{props.username}</h1>
        </div>
        <div className="message-text">
            {props.text}
        </div>
    </div>
)


export default Message