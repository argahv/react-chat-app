import React from 'react'

class SendMessageForm extends React.Component {

    constructor() {
        super()
        this.state = {
            message: ''
        }
    }

    handleChange = (e) => {
        console.log(e.target.value)
        this.setState({
            message: e.target.value
        })
    }

    handleSubmit =(e)=>{
        e.preventDefault()
        this.props.sendMessage(this.state.message)
        this.setState({
            message:''
        })
    }

    render() {
        return (
            <form className="send-message-form" onSubmit={this.handleSubmit}>
                <input
                    value={this.state.message}
                    onChange={this.handleChange}
                    placeholder="Type your message and hit ENTER"
                    type="text" />
            </form>
        )
    }
}

export default SendMessageForm