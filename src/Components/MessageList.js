import React from 'react'
import Message from './Message'
import ReactDOM from 'react-dom'

class MessageList extends React.Component {

    componentWillUpdate() {
        const node = ReactDOM.findDOMNode(this)
        this.shouldScrollToBottom = node.scrollTop + node.clientHeight + 200 >= node.scrollHeight
    }

    componentDidUpdate() {
        if (this.shouldScrollToBottom) {
            const node = ReactDOM.findDOMNode(this)
            node.scrollTop = node.scrollHeight
        }
    }


    render() {
        return (
            <div className="message-list">

                {/* {this.props.joinedRooms.roomId === this.props.rooms.roomId ?
                    this.props.joinedRooms.map(room => {
                        return (
                            <p key={room.roomId}>{room.name}</p>
                        )
                    }) : <p>No Groups available</p>} */}

                {this.props.messages.map((message, index) => {
                    return (
                        <Message key={index} username={message.senderId} text={message.text} />
                    )
                })}
            </div>
        )
    }
}

export default MessageList